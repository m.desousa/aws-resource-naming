output "aws_ec2_instance" {
  description = "Generated name for AWS EC2 instance"
  value       = random_id.aws_ec2_instance.hex
}

output "aws_s3_bucket" {
  description = "Generated name for AWS S3 bucket"
  value       = random_id.aws_s3_bucket.hex
}

output "aws_lambda_function_name" {
  description = "Generated name for AWS Lambda function"
  value       = random_id.aws_lambda_function.hex
}
