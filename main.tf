locals {
  application = trim(lower(var.application), " ")
  company     = trim(lower(var.company), " ")
  environment = trim(lower(var.environment), " ")
  squad       = trim(lower(var.squad), " ")
  prefix      = trim(lower("${var.company}-${var.squad}-${var.application}-${var.environment}"), " ")
}

resource "random_id" "aws_ec2_instance" {
  byte_length = 4
  prefix      = "ec2-${local.prefix}-"
  keepers = {
    application = local.application
    company     = local.company
    environment = local.environment
    squad       = local.squad
  }
}

resource "random_id" "aws_s3_bucket" {
  byte_length = 4
  prefix      = "s3-${local.prefix}-"
  keepers = {
    application = local.application
    company     = local.company
    environment = local.environment
    squad       = local.squad
  }
}

resource "random_id" "aws_lambda_function" {
  byte_length = 4
  prefix      = "lambda-${local.prefix}-"
  keepers = {
    application = local.application
    company     = local.company
    environment = local.environment
    squad       = local.squad
  }
}
