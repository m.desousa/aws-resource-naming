# AWS resource naming

This Terraform module generate name for common resources on AWS like EC2 instances, S3 buckets or Lambda functions.

The goal is to integrate this module on your Terraform projects to easily keep a simple naming policy on your AWS resources.

## Usage

```r
module "<module_name>" {
  source = "git::ssh://git@gitlab.com:m.desousa/aws-resource-naming.git"
  Informations = {
    CompanyName     = "Contoso"
    ApplicationName = "MyApp"
    Environment     = "Prod"
    SquadName       = "DevOps"
    Counts = {
      EC2Count    = 1
      S3Count     = 2
      LambdaCount = 5
    }
  }
}
```

This module output gives you a list of generated names to use.

## Example

```r
# Values
Information = {
  CompanyName     = "Contoso"
  ApplicationName = "MyApp"
  Environment     = "Devel"
  SquadName       = "TeamRocket"
  Counts = {
    EC2Count    = 1
    S3Count     = 2
    LambdaCount = 5
  }
}
```

```r
# Outputs
aws_ec2_instance_name = [
  "ec2-contoso-teamrocket-myapp-devel-literate-troll",
]
aws_lambda_function_name = [
  "lambda-contoso-teamrocket-myapp-devel-uncommon-grouper",
  "lambda-contoso-teamrocket-myapp-devel-climbing-rattler",
  "lambda-contoso-teamrocket-myapp-devel-primary-crappie",
  "lambda-contoso-teamrocket-myapp-devel-climbing-swift",
  "lambda-contoso-teamrocket-myapp-devel-flexible-blowfish",
]
aws_s3_bucket_name = [
  "s3-contoso-teamrocket-myapp-devel-trusting-buzzard",
  "s3-contoso-teamrocket-myapp-devel-thankful-treefrog",
]
```
