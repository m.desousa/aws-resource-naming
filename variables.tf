variable "application" {
  description = "Name of the application"
  type        = string
}

variable "environment" {
  description = "Name of the environment"
  type        = string
}

variable "squad" {
  description = "Name of the squad"
  type        = string
}

variable "company" {
  description = "Name of the company"
  type        = string
}
